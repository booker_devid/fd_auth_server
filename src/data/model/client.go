/*
Client model for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru

all word:
id - индентификатор
tok - token - токен
phe	- phone - телефон
rtg - rating -рэйтинг
cod - code verification - код подтверждения
dte - date - дата/время
nme - name - имя
sme - surname - фамилия
mme - middlename - отчество
usr - user - пользователь
cnt - contact - контакт
msg - message - сообщение
cty - city - город
nmc - full name city - полное наименование города
nen - name city of english - наименование на английском
nsh - short name city - короткое название города
lat - latitude - долгота
lon - longitude - широта
ord - order - заказ
por - plan order - запланированный заказ
tpl - time plan - время планирования
trp - trip - поездка
plc - place - место
drv - driver - водитель
car - автомобиль
clr - color - цвет
mrk - mark - марка
cmt - comment - комментарий
prc - price - цена
sta - status - статус
num - number - номер
sex - sex - пол
img - image get - ссылка для загрузки фотографии
imp - image put - ссылка здя выгрузки фотографии
yrs - years - возраст
smk - smoking - курение в солоне
fup - fast up auto - быстрая подача автомобиля
pay - pay system - система оплаты
bby - babych - детское кресло
key - смс ключ
tit - title - заголовок
val - value - содержимое
hon - housenumber - номер дома
str - street - улица
typ - type - тип
prt - Partner
ani - animal - животное
opt - option - параметры
reg - region - регион
pch - porch - подъезд
cme - comment exist - существования комментария
ver - verification - проверка
bgn-dte - begin date - дата начала
end-dte - end date - дата окончания
num-doc - document number - номер документа
ser-doc - document serial - серия документа
*/

package model

//Token - model for update or load token of the client
type Token struct {
	Token string `json:"tok"`
}

//Key - model for update or load key from sms for registration or recovery pass of user
type Key struct {
	Key string `json:"key"`
}

//Sms - model for update or load token of the client
type Sms struct {
	Code string `json:"cod"`
}

//City - model for update or load of th cities
type City struct {
	City []CityModel `json:"cty"`
}

//One
type One struct {
	Value string `json:"value"`
}

//GetUserData - Take user data
type GetUserData struct {
	Token         string    `json:"tok"`
	Driver        bool      `json:"drv"`
	DriverProfile bool      `json:"drv-prf"`
	Phone         string    `json:"phe"`
	UserName      string    `json:"nme"`
	City          CityModel `json:"cty"`
	Rating        int       `json:"rtg"`
	DateB         string    `json:"dte"`
	Sex           bool      `json:"sex"`
	ImageStatus   string    `json:"img"`
}

//CityModel - Take data of city
type CityModel struct {
	NameCity   string  `json:"nmc"`
	NameCityEn string  `json:"nen"`
	NameCitySh string  `json:"nsh"`
	Lat        float64 `json:"lat"`
	Lon        float64 `json:"lon"`
	Price      int     `json:"prc"`
}

//SetUserData - Registration user in system
type SetUserData struct {
	Token       string `json:"tok"`
	Phone       string `json:"phe"`
	Password    string `json:"pwd"`
	City        string `json:"cty"`
	UserName    string `json:"nme"`
	DateB       string `json:"dte"`
	Sex         bool   `json:"sex"`
	ImageStatus bool   `json:"img"`
}

//Start - starture date for client
type Start struct {
	Title string `json:"tit"`
	Value string `json:"val"`
	Image string `json:"img"`
}

//UpdateUserData -
type UpdateUserData struct {
	Token      string
	Phone      string
	Username   string
	Secondname string
	Middlname  string
	Password   string
	City       string
	Sex        string
	DayB       string
}

//UserForFCM -
type UserForFCM struct {
	UserName string
	Phone    string
	UID      string
}

/*
Session data - it's base data for client app.
*/

//SessionData - Struct for loading data of client
type SessionData struct {
	Token       string      `json:"token"`
	LastSession bool        `json:"last-session"`
	General     GeneralData `json:"general"`
	Driver      DriverData  `json:"driver"`
}

//GeneralData - Data for master object Client
type GeneralData struct {
	Phone       string            `json:"phone"`
	UserName    string            `json:"username"`
	Rating      int               `json:"rating"`
	DateB       string            `json:"date-bir"`
	Sex         bool              `json:"sex"`
	ImageStatus string            `json:"img"`
	City        CityData          `json:"city"`
	Orders      []OrderClientData `json:"orders"`
}

//CityData - Take data of city
type CityData struct {
	NameCity   string  `json:"name-city"`
	NameCityEn string  `json:"name-en"`
	NameCitySh string  `json:"name-short"`
	Lat        float64 `json:"lat"`
	Lon        float64 `json:"lon"`
	Price      int     `json:"price"`
}

type Citis struct {
	City []CityData `json:"cty"`
}

//DriverData - Data for slave object Driver
type DriverData struct {
	Exist        bool          `json:"exist"`
	Name         string        `json:"username"`
	Surname      string        `json:"surname"`
	Middlename   string        `json:"middle-name"`
	Verification bool          `json:"verification"`
	Car          CarData       `json:"car"`
	Rent         RentData      `json:"rent"`
	Orders       []OrderDriver `json:"orders"`
}

//CarData - Data auto
type CarData struct {
	Exist        bool      `json:"exist"`
	Color        ColorAuto `json:"color"`
	Mark         string    `json:"mark"`
	NumReg       string    `json:"reg-number"`
	Verification bool      `json:"confirmed"`
}

type ColorAuto struct {
	Title string `json:"title"`
	Value string `json:"value"`
}

//RentData - Data rent auto add parameters idCabinet and Name Cabinet
type RentData struct {
	IDCabinet   string  `json:"cbn"`
	NameCabinet string  `json:"nme"`
	Car         CarData `json:"car"`
}

//OrderClientData - Data order for client
type OrderClientData struct {
	ID        string      `json:"id"`
	Status    string      `json:"status"`
	InCity    bool        `json:"in-city"` //True - in City/False - out City
	Places    []PlaceData `json:"places"`
	AddOption OptionData  `json:"parametrs"`
	Driver    DriverOrder `json:"driver"`
}

//OrderDriverData - Data order for driver
type OrderDriver struct {
	ID        string      `json:"id"`
	Status    string      `json:"status"`
	InCity    bool        `json:"in-city"` //True - in City/False - out City
	Places    []PlaceData `json:"places"`
	AddOption OptionData  `json:"parametrs"`
}

//OrderCreatedData - Data order for driver
type OrderCreatedData struct {
	ID        string      `json:"id"`
	Name      string      `json:"username"`
	Status    string      `json:"status"`
	InCity    bool        `json:"in-city"` //True - in City/False - out City
	Places    []PlaceData `json:"places"`
	AddOption OptionData  `json:"parametrs"`
}

//DriverOrderData - Data Driver fir Order
type DriverOrder struct {
	Name     string           `json:"username"`
	Phone    string           `json:"phone"`
	Duration string           `json:"duration"`
	Img      string           `json:"img"`
	Car      CarOrderData     `json:"car"`
	Partner  PartnerOrderData `json:"partner"`
}

//CarOrderData
type CarOrderData struct {
	Mark   string    `json:"mark"`
	Color  ColorAuto `json:"color"`
	Number string    `json:"reg-number"`
}

//PartnerOrderData
type PartnerOrderData struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

//OptionData  - for OrderModelForSend
type OptionData struct {
	Price     int    `json:"price"`
	Babych    bool   `json:"babych"`
	Smoking   bool   `json:"smoking"`
	FastUp    bool   `json:"fast-up"`
	Comment   string `json:"comment"`
	Animal    bool   `json:"animals"`
	PlanOrder bool   `json:"plan-order"`
	PlanTime  string `json:"date-plan"`
	SystemPay bool   `json:"pay"`
}

//PlaceData - Data Place for order
type PlaceData struct {
	City      string  `json:"city"`
	Addr      string  `json:"addr"`
	TypePorch int     `json:"type-porch"`
	Porch     int     `json:"porch"`
	Lat       float64 `json:"lat"`
	Lon       float64 `json:"lon"`
}
