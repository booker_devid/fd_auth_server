package model

/*
Drvier model for client API.
Author: GuselnikovVasiliy/BookerDevid mr.wgw@yandex.ru
*/

//SetDriverData - registration driver in system
type SetDriverData struct {
	Token      string `json:"tok"`
	Name       string `json:"nme"`
	Surname    string `json:"sme"`
	Middlename string `json:"mme"`
	Serial     string `json:"ser-doc"`
	Number     string `json:"num-doc"`
}

//GetDriverData  - for keep driver data
type GetDriverData struct {
	Exist        bool       `json:"ext"`
	Name         string     `json:"nme"`
	Surname      string     `json:"sme"`
	Middlename   string     `json:"mme"`
	Verification bool       `json:"ver"`
	Car          GetCarData `json:"car"`
	Rend         GetCarData `json:"rnt"`
}

//SetCarData - registration driver in system
type SetCarData struct {
	Token  string `json:"tok"`
	Color  string `json:"clr"`
	Mark   string `json:"mrk"`
	NumReg string `json:"num"`
}

//GetCarData - for keep car data
type GetCarData struct {
	Exist        bool   `json:"ext"`
	Color        string `json:"clr"`
	Mark         string `json:"mrk"`
	NumReg       string `json:"num"`
	Verification bool   `json:"ver"`
	Rend         bool   `json:"rnt"`
	IDCabinet    string `json:"cbn"`
	NameCabinet  string `json:"nme"`
}

//GetCarsData - for load data cars driver
type GetCarsData struct {
	Car  GetCarData `json:"car"`
	Rend GetCarData `json:"rnt"`
}

//Properties - it's model data for work with colors and marks
type Properties struct {
	ID    string `json:"id"`
	Value string `json:"val"`
	Code  string `json:"cod"`
}
