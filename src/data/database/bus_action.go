package database

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"../../utility"
	"../firebase"
	"../model"
	pgx "github.com/jackc/pgx"
)

//BusHandlersVerificationAction - Handler for other actions is not order handler
func (db *Database) BusHandlersVerificationAction(errCh chan<- string) {
	pgconfListenString := strings.Split(db.connectionStrPG, "&")
	confConnListen, err := pgx.ParseConfig(pgconfListenString[0])
	dbl, err := pgx.ConnectConfig(context.Background(), confConnListen)
	if err != nil {
		errCh <- utility.ErrorHandler(errorPQX+" NewListen Verification ", err).Error()
		return
	}
	for {
		_, err = dbl.Exec(context.Background(), "LISTEN service")
		if err != nil {
			errCh <- utility.ErrorHandler(errorPQX+": ACQUIR! Error listen database ", err).Error()
			return
		}
		fmt.Println("Start listen @ServiceAction")
		for {
			notification, err := dbl.WaitForNotification(context.Background())
			if err != nil {
				errCh <- err.Error()
				break
			} else {
				fmt.Printf("\nGet notification:\n%v\n%v\n%v\n", notification.PID, notification.Channel, notification.Payload)
				go statusSigatureForService(db, notification.Payload)
			}
		}
		fmt.Println("End listen")
	}
}

func statusSigatureForService(db *Database, notifyData string) {
	dbConn, err := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	if err != nil {
		fmt.Println(utility.ErrorHandler(" PGQX!.PreparationDataActionCreated.Acquire ", err).Error())
	}
	status := strings.Split(notifyData, "*")
	switch status[0] {
	case "verification_driver":
		fmt.Println("Verification driver")
		PreparationVerificationDriver(status)
	case "verification_car":
		fmt.Println("Verification car")
		PreparationVerificationCar(status)
	case "load_order":
		fmt.Println("Load data order")
		PreparationDataLoadingOrder(dbConn.Conn(), status)
	case "short_call":
		fmt.Println("Order for all drivers")
		PreporationPushForDriverInCity(dbConn.Conn())
	}
	return
}

//PreparationVerificationDriver - Preparation data verification driver. Type list - 0.-TypeAction * 1.-FB-Token * 2.-Verification * 3.-Comment
func PreparationVerificationDriver(data []string) {
	modelVerificationData := model.ActionVerificationData{}
	dataModel := model.VerificationData{}
	if len(data) > 3 {
		if data[2] == "t" {
			dataModel.Verification = true
		} else {
			dataModel.Verification = false
		}
		dataModel.Comment = data[3]
	}
	modelVerificationData.Type = "verification"
	modelVerificationData.Object = "driver"
	modelVerificationData.Data = dataModel
	firebase.SendOneDataNotification(modelVerificationData, "Данные водителя проверены!", data[3], data[1])
}

//PreparationVerificationCar - Preparation data verification car. Type list - 0.-TypeAction * 1.-FB-Token * 2.-Verification * 3.-Comment
func PreparationVerificationCar(data []string) {
	if len(data) > 3 {
		modelVerificationData := model.ActionVerificationData{}
		dataModel := model.VerificationData{}
		if data[2] == "t" {
			dataModel.Verification = true
		} else {
			dataModel.Verification = false
		}
		dataModel.Comment = data[3]
		modelVerificationData.Type = "verification"
		modelVerificationData.Object = "car"
		modelVerificationData.Data = dataModel
		firebase.SendOneDataNotification(modelVerificationData, "Данные автомобиля проверены!", data[3], data[1])
	}
}

//PreporationPushForDriverInCity - preporated data for push of about creating a orders
func PreporationPushForDriverInCity(db *pgx.Conn) {
	query, err := db.Query(context.Background(), "SELECT tok.fb_token FROM drivers AS dr, users AS us, tokens AS tok WHERE us.id = dr.id AND us.driver = TRUE AND tok.id = dr.id AND tok.fb_token != ''")
	if err != nil {
		fmt.Println("Panik PreporationPushForDriverInCity")
		return
	}
	var token string
	if query.Next() {
		query.Scan(&token)
		//
		modelNotifyData := model.ActionNotifyData{}
		modelNotifyData.Type = "notify"
		modelNotifyData.Object = "order"
		//
		firebase.SendOneDataNotification(modelNotifyData, "Поступил новый заказ!", "Новый заказ в городе Димитровград!", token)
	}
}

//PreparationLoadOrder - Preparation data orders user. Type list - 0.-TypeAction * 1.-FB-Token * 2.-ID-User
func PreparationLoadOrder(data []string) {

}

//PreparationDataLoadingOrder - Preparation data for loadig orders for user. Type list - 0.-TypeAction * 1.-ID-Order 2.- ID-User 3.- FirebaseToken
func PreparationDataLoadingOrder(db *pgx.Conn, data []string) { //Add
	if len(data) > 1 {
		fmt.Println("Data - ", data[0], data[1], data[2], data[3])
		var id, statusOrder, username, phone, cabinet, idCar string
		var typeUserinOrder, img bool
		var rating int
		var placesData []byte //dataOrder
		var modelActionLoadOrder model.ActionLoadOrder
		modelActionLoadOrder.Type = "orders"
		modelActionLoadOrder.Object = "load"
		if err := db.QueryRow(context.Background(), "SELECT us.id, us.phone, us.rating, us.username, us.img, CASE WHEN (tr.client = us.id) THEN TRUE ELSE FALSE END AS client, tr.status_trip FROM users AS us, (SELECT client, driver, status_trip FROM trip_client WHERE id = $1) AS tr WHERE us.id = tr.client OR us.id = tr.driver AND us.id = $2",
			data[1], data[2]).Scan(&id, &phone, &rating, &username, &img, &typeUserinOrder, &statusOrder); err != nil {
			fmt.Println(err.Error())
			return
		}
		places := []model.Place{}
		option := model.OptionData{}
		if typeUserinOrder == true { // type true - Client| false - Driver
			switch statusOrder {
			case "created", "plan":
				{
					clientData := model.ReadyOrderModelCreated{}
					clientData.Actor = "client"
					clientData.IDOrder = data[1]
					if err := db.QueryRow(context.Background(), "SELECT places, in_city, babych, smoking, fastup, animal, comment, pay, price FROM trip_client WHERE id = $1",
						data[1]).Scan(&placesData, &clientData.InCity, &option.Babych, &option.Smoking, &option.FastUp, &option.Animal, &option.Comment, &option.SystemPay, &option.Price); err != nil {
						fmt.Println("Z2", err.Error())
						return
					}
					fmt.Println(placesData, clientData.InCity, option.Babych, option.Smoking, option.FastUp, option.Animal, option.Comment, option.SystemPay, option.Price)
					json.Unmarshal(placesData, &places)
					clientData.Places = places
					clientData.AddOption = option
					modelActionLoadOrder.Data = clientData
					// dataOrder, _ = json.Marshal(modelActionLoadOrder)
				}
			case "start", "driver_ready", "begin_trip":
				{
					clientData := model.ReadyOrderModelClient{}
					clientData.Actor = "client"
					clientData.IDOrder = data[1]
					if err := db.QueryRow(context.Background(), "SELECT places, in_city, babych, smoking, fastup, animal, comment, pay, price, duration_waiting FROM trip_client WHERE id = $1",
						data[1]).Scan(&placesData, &clientData.InCity, &option.Babych, &option.Smoking, &option.FastUp, &option.Animal, &option.Comment, &option.SystemPay, &option.Price, &clientData.Driver.Duration); err != nil {
						fmt.Println("Z2", err.Error())
						return
					}
					json.Unmarshal(placesData, &places)
					fmt.Println(clientData.InCity, option.Babych, option.Smoking, option.FastUp, option.Animal, option.Comment, option.SystemPay, option.Price, clientData.Driver.Duration)
					if err := db.QueryRow(context.Background(),
						"SELECT us.surname || ' ' || us.username AS name, CASE WHEN (dr.car_arend != NULL AND dr.car_arend != '') THEN dr.car_arend ELSE dr.car END AS rend FROM users AS us, drivers AS dr WHERE us.id = $1 AND dr.id = us.id",
						clientData.Driver.IDDriver).Scan(&clientData.Driver.Name, &idCar); err != nil {
						fmt.Println("User data ", err.Error())
						return
					}
					if err := db.QueryRow(context.Background(),
						"SELECT mk.mark, cl.color, cl.code, car.num_reg, CASE WHEN (car.owner_cabinet != NULL AND car.owner_cabinet != '') THEN car.owner_cabinet ELSE 'no' END AS cab FROM cars AS car, color_auto AS cl, mark AS mk WHERE car.id = $1 AND cl.id = car.color AND mk.id = car.mark",
						idCar).Scan(&clientData.Driver.Car.Mark, &clientData.Driver.Car.Color.Title, &clientData.Driver.Car.Color.Value, &clientData.Driver.Car.Number, &cabinet); err != nil {
						fmt.Println("Car data - ", err.Error())
						return
					}
					fmt.Println(clientData.Driver.Car.Mark, clientData.Driver.Car.Color.Title, clientData.Driver.Car.Color.Value, clientData.Driver.Car.Number, cabinet)
					if len(cabinet) > 3 && cabinet != "no" {
						if err := db.QueryRow(context.Background(), "SELECT id, title_ps FROM cabinet_ps WHERE id = $1",
							cabinet).Scan(&clientData.Driver.Partner.ID, &clientData.Driver.Name); err != nil {
							fmt.Println("cabinet data - ", err.Error())
							return
						}
					}
					clientData.Places = places
					clientData.AddOption = option
					modelActionLoadOrder.Data = clientData
					// dataOrder, _ = json.Marshal(modelActionLoadOrder)
				}
			}
		} else {
			switch statusOrder {
			case "plan_add_driver", "start_order", "driver_ready", "begin_trip":
				{
					var idUser, nameUser, phoneUser string
					var ratingUser int
					driverData := model.ReadyOrderModelDriver{}
					if err := db.QueryRow(context.Background(), "SELECT us.id, us.phone, us.rating, us.username, img FROM users AS us, (SELECT client FROM trip_client WHERE id = $1) AS tr WHERE us.id = tr.client",
						data[1]).Scan(&idUser, &phoneUser, &ratingUser, &nameUser, &img); err != nil {
						fmt.Println(err.Error())
						return
					}
					if err := db.QueryRow(context.Background(), "SELECT places, in_city, babych, smoking, fastup, animal, comment, pay, price, duration_waiting FROM trip_client WHERE id = $1",
						data[1]).Scan(&placesData, &driverData.InCity, &option.Babych, &option.Smoking, &option.FastUp, &option.Animal, &option.Comment, &option.SystemPay, &option.Price); err != nil {
						fmt.Println("Z2", err.Error())
						return
					}
					json.Unmarshal(placesData, &places)
					driverData.Actor = "driver"
					driverData.IDOrder = data[1]
					driverData.Client.IDClient = idUser
					driverData.Client.Name = nameUser
					driverData.Client.Phone = phoneUser
					driverData.Client.Rating = ratingUser
					if img {
						driverData.Client.Img = fmt.Sprintf("https://scar.metro86.ru:4740/media/usr?phone=%v&token=%v", phone, takeMD5Sol(fmt.Sprintf("%v", time.Now().Unix())))
					} else {
						driverData.Client.Img = ""
					}
					driverData.Places = places
					modelActionLoadOrder.Data = driverData
					// dataOrder, _ = json.Marshal(modelActionLoadOrder)
				}
			}
		}
		fmt.Println("Write data in FB")
		firebase.SendOneData(modelActionLoadOrder, data[3])
	}
}
