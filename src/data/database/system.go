package database

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"../model"
)

//CheckAuthAgent - return of auth check{0-good,1-autorithation error,2-block user}
func (db *Database) CheckAuthAgent(phone, pwd string) (string, int, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idUser, scanPass string
	var block bool
	_ = dbConn.QueryRow(context.Background(), "SELECT id, pwd, block_agent FROM agents WHERE phone = $1", phone).Scan(&idUser, &scanPass, &block)
	if block == true {
		return "", 2, nil
	}
	if idUser == "" || scanPass == "" {
		return "", 1, nil
	}
	if pwd != scanPass {
		return "", 1, nil
	}
	return idUser, 0, nil
}

//CheckAuthAdmin - return of auth check{0-good,1-autorithation error,2-block user}
func (db *Database) CheckAuthAdmin(phone, pwd string) (string, int, int, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var idUser, scanPass string
	var priv int
	_ = dbConn.QueryRow(context.Background(), "SELECT id, pwd, privileges FROM dys1984 WHERE username = $1", phone).Scan(&idUser, &scanPass, &priv)
	if idUser == "" || scanPass == "" {
		return "", 0, 1, nil
	}
	if pwd != scanPass {
		return "", 0, 1, nil
	}
	fmt.Println("Data ", idUser, scanPass, priv)
	return idUser, priv, 0, nil
}

//SessionAgentData
func (db *Database) SessionAgentData(idUser string) ([]byte, error) {

	return nil, nil
}

//SessionAdminData
func (db *Database) SessionAdminData(idUser string, priv int) ([]byte, error) { //priv - 0 max, 1 min
	var modelAdmin model.Token
	var err error
	if priv == 0 {
		modelAdmin.Token, err = db.TakeTokenA(idUser, " ")
		if err != nil {
			return nil, err
		}
	}
	fmt.Println("Token - ", modelAdmin.Token)
	data, err := json.Marshal(modelAdmin)
	return data, nil
}

//RegistrationAgentInDatabase
func (db *Database) RegistrationAgentInDatabase(modelAgentData model.SetAgentData) error { //priv - 0 max, 1 min
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	idCity, err := db.TakeIDCity(modelAgentData.City)
	if err != nil {
		return nil
	}
	unix := time.Now().Unix()
	dbConn.QueryRow(context.Background(),
		"SELECT f_new_agent($1,$2,$3,$4,$5,$6,$7,$8)",
		modelAgentData.Username,   //username
		modelAgentData.Surname,    //surname
		modelAgentData.Middlename, //middlename
		modelAgentData.Phone,      //phone
		modelAgentData.Pwd,        //pass
		idCity,                    //city
		unix,                      //date-reg
		modelAgentData.DateBir,    //date_bir
	).Scan()
	return nil
}

//RegistrationCabinetPS
func (db *Database) RegistrationCabinetPS(modelCabinetData model.SetCabinetDataData) (int, error) { //0 - good, 1- limit or db error, 2- error
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan string
	idCity, err := db.TakeIDCity(modelCabinetData.City)
	if err != nil {
		return 0, nil
	}
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_new_cabinet_ps($1,$2,$3,$4,$5,$6)",
		modelCabinetData.TitleOut, //title out
		modelCabinetData.TitleIn,  //title in
		idCity,                    //city
		modelCabinetData.Lat,      //lat
		modelCabinetData.Lon,      //lon
		modelCabinetData.Addr,     //addr
	).Scan(&resultScan); err != nil {
		return 2, err
	}
	if resultScan == "" || resultScan == "LIMIT" {
		return 1, nil
	}
	return 0, nil
}

//RegistrationCabinetFD
func (db *Database) RegistrationCabinetFD(modelCabinetData model.SetCabinetDataData) (int, error) { //0 - good, 1- limit or db error, 2- error
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan string
	idCity, err := db.TakeIDCity(modelCabinetData.City)
	if err != nil {
		return 0, nil
	}
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_new_cabinet_fd($1)", idCity).Scan(&resultScan); err != nil {
		return 2, err
	}
	if resultScan == "" || resultScan == "LIMIT" {
		return 1, nil
	}
	return 0, nil
}

//RegistrationSessionCB
func (db *Database) RegistrationSessionCB(modelSessionData model.SetSessionData) (int, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan string
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_new_session_dr($1,$2,$3)").Scan(&resultScan); err != nil {
		return 2, err
	}
	if resultScan == "EXIST" {
		return 1, nil
	}
	return 0, nil
}

//EnableSessionInDatabase -
func (db *Database) EnableSessionInDatabase(idSession string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan bool
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_enable_session($1)", idSession).Scan(&resultScan); err != nil {
		return err
	}
	return nil
}

//DisableSessionInDatabase -
func (db *Database) DisableSessionInDatabase(idSession string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan bool
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_disable_session($1)", idSession).Scan(&resultScan); err != nil {
		return err
	}
	return nil
}

//GetCabinetSessionInDatabase -
func (db *Database) GetCabinetSessionInDatabase(idCabinet string) ([]model.GetSessionData, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var sessions []model.GetSessionData
	query, err := dbConn.Query(context.Background(), "SELECT id, cabinet, duration, price,  ", idCabinet)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	for query.Next() {
		var session model.GetSessionData
		err = query.Scan(&session.ID, &session.Cabinet, &session.Duration, &session.Price, &session.Active)
		if err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
		sessions = append(sessions, session)
	}

	return nil, nil
}

//GetAllSessionInDatabase -
func (db *Database) GetAllInCitySessionInDatabase(idCity string) ([]model.GetSessionData, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var sessions []model.GetSessionData
	query, err := dbConn.Query(context.Background(), "SELECT sdr.id, sdr.cabinet, sdr.duration, sdr.price, srd.active FROM sessions_dr AS sdr, (SELECT * FROM cabinet_ps WHERE city = $1) AS cb WHERE sdr.cabinet = cb.id", idCity)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	for query.Next() {
		var session model.GetSessionData
		err = query.Scan(&session.ID, &session.Cabinet, &session.Duration, &session.Price, &session.Active)
		if err != nil {
			fmt.Println(err.Error())
			return nil, err
		}
		sessions = append(sessions, session)
	}

	return nil, nil
}

//GetAllCabinetCity -
func (db *Database) GetAllCabinetCity(idCity string) ([]model.CabinetData, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var cabinets []model.CabinetData
	query, err := dbConn.Query(context.Background(),
		"SELECT id, title_ps, title_scar, city, del_cabinet, del_commit, limit_session, limit_session_out, lat, lon, addr FROM cabinet_ps WHERE city = $1",
		idCity)
	if err != nil {
		return cabinets, err
	}
	for query.Next() {
		var cabinetData model.CabinetData
		query.Scan(&cabinetData.ID, &cabinetData.TitleOut, &cabinetData.TitleIn, &cabinetData.City, &cabinetData.Del, &cabinetData.DelComment, &cabinetData.Limit, &cabinetData.LimitNum, &cabinetData.Lat, &cabinetData.Lon, &cabinetData.Addr)
		cabinets = append(cabinets, cabinetData)
	}
	return cabinets, nil
}

//GetAllCabinetAgent -
func (db *Database) GetAllCabinetAgent(idCabinet string) ([]model.CabinetData, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var cabinets []model.CabinetData
	query, err := dbConn.Query(context.Background(),
		"SELECT id, title_ps, title_scar, city, del_cabinet, del_commit, limit_session, limit_session_out, lat, lon, addr FROM cabinet_ps WHERE id = $1",
		idCabinet)
	if err != nil {
		return cabinets, err
	}
	for query.Next() {
		var cabinetData model.CabinetData
		query.Scan(&cabinetData.ID, &cabinetData.TitleOut, &cabinetData.TitleIn, &cabinetData.City, &cabinetData.Del, &cabinetData.DelComment, &cabinetData.Limit, &cabinetData.LimitNum, &cabinetData.Lat, &cabinetData.Lon, &cabinetData.Addr)
		cabinets = append(cabinets, cabinetData)
	}
	return cabinets, nil
}

//DeleteCabinetInDatabase -
func (db *Database) DeleteCabinetInDatabase(idCabinet, commit string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var scanIdCabinet string
	if err := dbConn.QueryRow(context.Background(), "SELECT f_del_cabinet_ps($1,$2)", idCabinet, commit).Scan(&scanIdCabinet); err != nil {
		return err
	}
	return nil
}

//RecoveryCabinetInDatabase -
func (db *Database) RecoveryCabinetInDatabase(idCabinet string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var scanIdCabinet string
	if err := dbConn.QueryRow(context.Background(), "SELECT f_recovery_cabinet_ps($1)", idCabinet).Scan(&scanIdCabinet); err != nil {
		return err
	}
	return nil
}

//UpdateCabinetInDatabase -
func (db *Database) UpdateCabinetInDatabase(cabinet model.CabinetData) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var resultScan string
	idCity, err := db.TakeIDCity(cabinet.City)
	if err != nil {
		return nil
	}
	if err := dbConn.QueryRow(context.Background(),
		"SELECT f_update_cabinet_ps($1,$2,$3,$4,$5,$6,$7)",
		cabinet.ID,       //id
		cabinet.TitleOut, //title out
		cabinet.TitleIn,  //title in
		idCity,           //city
		cabinet.Lat,      //lat
		cabinet.Lon,      //lon
		cabinet.Addr,     //addr
	).Scan(&resultScan); err != nil {
		return err
	}
	return nil
}

//GetSessionForDriver -
func (db *Database) GetSessionForDriver(idCity string) ([]model.SessionForDrivers, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var sessions []model.SessionForDrivers
	query, err := dbConn.Query(context.Background(),
		"SELECT sdr.id, cb.title_ps, sdr.duration, sdr.price, CASE WHEN bool_or(cb.id like '%FD%') THEN TRUE ELSE FALSE END AS promo FROM sessions_dr AS sdr, (SELECT id, title_ps FROM cabinet_ps WHERE city = $1) AS cb WHERE sdr.cabinet = cb.id AND sdr.active = TRUE GROUP BY sdr.id, cb.title_ps, sdr.duration, sdr.price",
		idCity)
	if err != nil {
		return sessions, err
	}
	for query.Next() {
		var session model.SessionForDrivers
		query.Scan(&session.ID, &session.Cabinet, &session.Duration, &session.Price, &session.Promocod)
		sessions = append(sessions, session)
	}
	return sessions, nil
}

func (db *Database) GetListCars(all, verification bool) ([]byte, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var listCar []model.CarSystemModel
	var car model.CarSystemModel
	if !all {
		if verification {
			//all driver
			query, err := dbConn.Query(context.Background(), "SELECT cr.id, cr.num_reg, mk.mark, cl.color, cr.verification, us.phone FROM cars AS cr, mark AS mk, color_auto AS cl, users AS us WHERE cr.verification = FALSE AND COALESCE(cr.owner_user, '') != '' AND us.id = cr.owner_user AND mk.id = cr.mark AND cl.id = cr.color")
			if err != nil {
				return nil, err
			}
			for query.Next() {
				query.Scan(&car.ID, &car.RegistrationNmber, &car.Mark, &car.Color, &car.VerificationData, &car.Owner)
				listCar = append(listCar, car)
			}
		}
	} else {
		//all car
		query, err := dbConn.Query(context.Background(), "SELECT cr.id, cr.num_reg, mk.mark, cl.color, cr.verification FROM cars AS cr, mark AS mk, color_auto AS cl WHERE mk.id = cr.mark AND cl.id = cr.color;")
		if err != nil {
			return nil, err
		}
		for query.Next() {
			query.Scan(&car.ID, &car.RegistrationNmber, &car.Mark, &car.Color, &car.VerificationData)
			listCar = append(listCar, car)
		}
	}
	var jsonModel model.ListElementForSystemModel
	jsonModel.Count = len(listCar)
	jsonModel.List = listCar
	return json.Marshal(jsonModel)
}

func (db *Database) GetListUsers(all, driver, verification bool) ([]byte, error) {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	var listUser []model.UserSystemModel
	var user model.UserSystemModel
	if !all {
		if driver && verification {
			//all driver
			query, err := dbConn.Query(context.Background(), "SELECT us.id, us.phone, COALESCE(us.surname,'') || ' ' || COALESCE(us.username,'') || ' ' || COALESCE(us.middlename, '') AS name, CASE WHEN us.sex THEN 'муж.' ELSE 'жен.' END AS sex, dr.verification_driver, rg.region || ' ' || ct.city AS city FROM drivers AS dr, users AS us, cities AS ct, regions AS rg WHERE us.id = dr.id AND ct.id = us.city AND rg.id = ct.region ORDER BY us.date_reg")
			if err != nil {
				return nil, err
			}
			for query.Next() {
				query.Scan(&user.ID, &user.Phone, &user.Name, &user.Sex, &user.Verification, &user.City)
				listUser = append(listUser, user)
			}
		} else {
			//no verification driver
			query, err := dbConn.Query(context.Background(), "SELECT us.id, us.phone, COALESCE(us.surname,'') || ' ' || COALESCE(us.username,'') || ' ' || COALESCE(us.middlename, '') AS name, CASE WHEN us.sex THEN 'муж.' ELSE 'жен.' END AS sex, dr.verification_driver, rg.region || ' ' || ct.city AS city FROM drivers AS dr, users AS us, cities AS ct, regions AS rg WHERE dr.verification_driver != TRUE AND us.id = dr.id AND ct.id = us.city AND rg.id = ct.region ORDER BY us.date_reg")
			if err != nil {
				return nil, err
			}
			for query.Next() {
				query.Scan(&user.ID, &user.Phone, &user.Name, &user.Sex, &user.Verification, &user.City)
				listUser = append(listUser, user)
			}
		}
	} else {
		//all user
		query, err := dbConn.Query(context.Background(), "SELECT us.id, us.phone, COALESCE(us.surname,'') || ' ' || COALESCE(us.username,'') || ' ' || COALESCE(us.middlename, '') AS name, CASE WHEN us.sex THEN 'муж.' ELSE 'жен.' END AS sex, rg.region || ' ' || ct.city AS city FROM users AS us, cities AS ct, regions AS rg WHERE ct.id = us.city AND rg.id = ct.region ORDER BY us.date_reg")
		if err != nil {
			return nil, err
		}
		for query.Next() {
			query.Scan(&user.ID, &user.Phone, &user.Name, &user.Sex, &user.City)
			listUser = append(listUser, user)
		}
	}
	var jsonModel model.ListElementForSystemModel
	jsonModel.Count = len(listUser)
	jsonModel.List = listUser
	return json.Marshal(jsonModel)
}

func (db *Database) GetInfoAboutDriver() {

}

//SELECT us.id, us.phone, us.surname || ' ' || us.username || ' ' || us.middlename AS name, dr.verification_driver, rg.region || ' ' || ct.city AS city, cr.num_reg, mk.mark, cl.color FROM drivers AS dr, users AS us, cities AS ct, regions AS rg, cars AS cr, mark AS mk, color_auto AS cl WHERE us.id = dr.id AND ct.id = us.city AND rg.id = ct.region AND cr.id = dr.car AND mk.id = cr.mark AND cl.id = cr.color

//SetVerificationDriver -
func (db *Database) SetVerificationDriver(idUser string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "UPDATE drivers SET verification_driver = TRUE WHERE id = $1", idUser).Scan(&idUser)
	dbConn.Exec(context.Background(), "SELECT pg_notify('service','verification_driver*'||(SELECT fb_token FROM tokens WHERE id = $1)||'*true*Данные были подтверждены, теперь вы можете переключиться в режим водителя!')", idUser)
	return nil
}

//DeleteDriverDataUser -
func (db *Database) DeleteDriverDataUser(idUser string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "UPDATE cars SET owner_user = '' WHERE owner_user = $1", idUser).Scan(&idUser)
	dbConn.QueryRow(context.Background(), "DELETE FROM drivers WHERE id = $1", idUser).Scan(&idUser)
	dbConn.Exec(context.Background(), "SELECT pg_notify('service','verification_driver*'||(SELECT fb_token FROM tokens WHERE id = $1)||'*false*Данные не были подтверждены, обратитесь в поддержку или повторите регистрацию!')", idUser)
	return nil
}

//SetVerificationCar -
func (db *Database) SetVerificationCar(idUser string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "UPDATE cars SET verification = TRUE WHERE owner_user = $1", idUser).Scan(&idUser)
	dbConn.Exec(context.Background(), "SELECT pg_notify('service','verification_car*'||(SELECT fb_token FROM tokens WHERE id = $1)||'*true*Автомобиль был успешно подтвержден!')", idUser)
	return nil
}

//DeleteDriverDataCar -
func (db *Database) DeleteDriverDataCar(idUser string) error {
	dbConn, _ := db.dbConnectPool.Acquire(context.Background())
	defer dbConn.Release()
	dbConn.QueryRow(context.Background(), "UPDATE cars SET owner_user = '' WHERE owner_user = $1", idUser).Scan(&idUser)
	dbConn.QueryRow(context.Background(), "UPDATE drivers SET car = '' WHERE id = $1", idUser).Scan(&idUser)
	dbConn.Exec(context.Background(), "SELECT pg_notify('service','verification_car*'||(SELECT fb_token FROM tokens WHERE id = $1)||'*false*Автомобиль не был подтвержден, обратитесь в поддержку!')", idUser)
	return nil
}
