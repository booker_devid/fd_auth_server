package firebase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type fcmRequest struct {
	To               string       `json:"to"`
	NotificationConf notification `json:"notification"`
}

type notification struct {
	Title string `json:"title"`
	Body  string `json:"body"`
	Sound string `json:"sound"`
}

type fcmNotifyOne struct {
	To               string       `json:"to"`
	NotificationConf notification `json:"notification"`
	Data             interface{}  `json:"data"`
}

type fcmDataOne struct {
	To   string      `json:"to"`
	Data interface{} `json:"data"`
}

type fcmDataNotifyOne struct {
	To               string       `json:"to"`
	NotificationConf notification `json:"notification"`
	Data             interface{}  `json:"data"`
}

type fcmNotifySome struct {
	To               []string     `json:"to"`
	NotificationConf notification `json:"notification"`
	Data             interface{}  `json:"data"`
}

type fcmDataSome struct {
	To   []string    `json:"to"`
	Data interface{} `json:"data"`
}

//SendAll -
func SendAll(title string, body string, data interface{}, channel string) {
	message := &fcmNotifyOne{To: channel}
	// notify := notification{}
	// notify.Body = body
	// notify.Title = title
	// message.NotificationConf = notify
	message.Data = data
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

//SendSomeNotification -
func SendSomeNotification(title string, body string, tokens []string) {
	message := &fcmNotifySome{To: tokens}
	notify := notification{}
	notify.Body = body
	notify.Title = title
	notify.Sound = "default"
	message.NotificationConf = notify
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

//SendSomeData -
func SendSomeData(data interface{}, tokens []string) {
	message := &fcmDataSome{To: tokens}
	message.Data = data
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

//SendOneNotification -
func SendOneNotification(title string, body string, token string) {
	message := &fcmNotifyOne{To: token}
	notify := notification{}
	notify.Body = body
	notify.Title = title
	message.NotificationConf = notify
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

//SendOneData -
func SendOneData(data interface{}, token string) {
	message := &fcmDataOne{To: token}
	message.Data = data
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

//SendOneDataNotification -
func SendOneDataNotification(data interface{}, title string, body string, token string) {
	message := &fcmDataNotifyOne{To: token}
	notify := notification{}
	notify.Body = body
	notify.Title = title
	message.NotificationConf = notify
	message.Data = data
	binaryDataForSend, _ := json.Marshal(message)
	sendMessage(binaryDataForSend)
}

func sendMessage(data []byte) (map[string]interface{}, error) {
	req, err := http.NewRequest("POST", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "key=AAAAQuGkap0:APA91bE85dgg5fPX2FLK0XbVBjVL3-2Ti507sRn7GBkT3pTVAQubDImd4qoJaiA9Nu--dOL1Jc_s3-dj2c9LS9ITBAgeW7o8oGHnw_tSuIPe20KkkD_1z62uVJ1GCFA8ElDbWCYVQH9H")
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	return result, err
}

//Test temp functions

//SendTestMessage  - send message for users
func SendTestMessage(tokenFB, key string) error {
	// client := fcm.NewClient("AAAAQuGkap0:APA91bE85dgg5fPX2FLK0XbVBjVL3-2Ti507sRn7GBkT3pTVAQubDImd4qoJaiA9Nu--dOL1Jc_s3-dj2c9LS9ITBAgeW7o8oGHnw_tSuIPe20KkkD_1z62uVJ1GCFA8ElDbWCYVQH9H")
	message := &fcmRequest{To: tokenFB}
	notify := notification{}
	notify.Body = key
	notify.Title = "SMS key"
	message.NotificationConf = notify
	fmt.Println("Take Token")
	// data, err := json.Marshal(message)
	// if err != nil {
	// 	return err
	// }
	// fmt.Printf("%v\n", data)
	fmt.Println("To - ", tokenFB)
	fmt.Println("Title - ", notify.Title)
	fmt.Println("Body - ", notify.Body)
	//client.PushSingle(tokenFB, data)
	status, err := Send(message)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	fmt.Println(status)
	return nil
}

//Send -
func Send(msg *fcmRequest) (map[string]interface{}, error) {
	bytesRepresentation, err := json.Marshal(msg)
	if err != nil {
		log.Fatalln(err)
	}
	req, err := http.NewRequest("POST", "https://fcm.googleapis.com/fcm/send", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "key=AAAAQuGkap0:APA91bE85dgg5fPX2FLK0XbVBjVL3-2Ti507sRn7GBkT3pTVAQubDImd4qoJaiA9Nu--dOL1Jc_s3-dj2c9LS9ITBAgeW7o8oGHnw_tSuIPe20KkkD_1z62uVJ1GCFA8ElDbWCYVQH9H")
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	var result map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&result)
	return result, err
}
