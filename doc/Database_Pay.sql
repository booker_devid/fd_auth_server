
--Таблица promo_cod содержит в себе информацию о промокодах.
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"cods" - промокод. (Уникальное)
--"deduction" - % вычита от стоимости сессии, не действует на сессии партнеров.
--"count" - количество возможных активаций.
--"time_out" - дата выхода промокода из оборота.
DROP TABLE IF EXISTS promo_cod;
CREATE TABLE promo_cod (
id VARCHAR(15) NOT NULL PRIMARY KEY,
cods VARCHAR(12) NOT NULL,
deduction SMALLINT NOT NULL,
count_use SMALLINT NOT NULL,
time_out BIGINT,
UNIQUE(cods)
);
--Последовательность для таблицы: promo_cod
DROP SEQUENCE IF EXISTS promo_cod_id_seq;
CREATE SEQUENCE promo_cod_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Function add promo_cod
DROP FUNCTION IF EXISTS f_new_promo_cod(_cod VARCHAR(12), _deduction INTEGER, _count_use INTEGER, _time_out BIGINT);
CREATE OR REPLACE FUNCTION f_new_promo_cod(_cod VARCHAR(12), _deduction INTEGER, _count_use INTEGER, _time_out BIGINT) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'PRMCD';
SELECT nextval('promo_cod_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO promo_cod(id, cods, deduction, count_use, time_out) VALUES(_id, _cod, _deduction, _count_use, _time_out);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_promo_cod('745NV345A001', 10, 5, 15040393900);
SELECT f_new_promo_cod('745NV345A002', 50, 4, 15040393901);
SELECT f_new_promo_cod('745NV345A003', 100, 3, 15040393902);
SELECT * FROM promo_cod;

-- --Таблица services_ps содержит в себе информацию о услугах для партнеров по сотрудничетсву...
-- --Подразумевается продажа увеличения лимита независимых водителей на сессии партнера.
-- --бесплатно 15 (Независимых автомобилей)
-- --1520руб. Увелечение на 1 машин, длительность год. Не более 40
-- --бесплатно 9 (Личный автопарк обклееных автомобилей)
-- --2000руб. Увелечение на 1 машин, длительность - нет. Не более 24
-- --Максимум 64 автомобиля на 1 партнера.
-- --"id" - уникальный индетификатор. (Уникальное, внешний ключ)
-- --"duration" - длительность услуги
-- --"about" - описание услуги
-- --"price" - цена услуги
-- --"active" - активность услуги
-- --"city" - город действия услуги (Пустое поле означает все города) 
-- --"time_create" - время создания
-- --"title" - заголовок для отображения пользователю
-- --"about" - описание для отображения пользователю 
-- DROP TABLE IF EXISTS services_ps;
-- CREATE TABLE services_ps (
-- id VARCHAR(15) NOT NULL PRIMARY KEY,
-- duration BIGINT NOT NULL, -- 0 = all time
-- price NUMERIC NOT NULL  CHECK (price >= 0),
-- active BOOLEAN NOT NULL,
-- city VARCHAR(15) REFERENCES cities (id) ON DELETE SET NULL,
-- time_create BIGINT,
-- title VARCHAR(200) NOT NULL,
-- about TEXT
-- );
-- --Последовательность для таблицы: services_ps
-- CREATE SEQUENCE services_ps_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
-- --Function add services_ps
-- DROP FUNCTION IF EXISTS f_new_services_ps(
-- _duration BIGINT, 
-- _price NUMERIC, 
-- _active BOOLEAN, 
-- _city VARCHAR(15), 
-- _time_create BIGINT, 
-- _title VARCHAR(200), 
-- _about TEXT);
-- CREATE OR REPLACE FUNCTION f_new_services_ps(
-- _duration BIGINT, 
-- _price NUMERIC, 
-- _active BOOLEAN, 
-- _city VARCHAR(15), 
-- _time_create BIGINT, 
-- _title VARCHAR(200), 
-- _about TEXT) RETURNS VARCHAR(15) AS $$
-- DECLARE
-- _id VARCHAR(15);
-- _num_nextval VARCHAR(252);
-- BEGIN
-- _id = 'SRVPS';
-- SELECT nextval('services_ps_id_seq') INTO _num_nextval;
-- _id = CONCAT(_id,_num_nextval);
-- INSERT INTO services_ps(id, duration, price, active, city, time_create, title, about) 
-- VALUES(_id, _duration, _price, _active, _city, _time_create, _title, _about);
-- RETURN _id;
-- END;
-- $$ LANGUAGE plpgsql;
-- --Default:
-- INSERT INTO services_ps VALUES
-- ('SRVPS1',1800,1500,TRUE,'CT1',1571944444,'Место для машины','Место для водителей, на своих машинах. 
-- Купив данное место вы увеличиваете колличество мест, которые могут купить не привязанные водители.'),
-- ('SRVPS2',1800,1500,FALSE,'CT1',1571944444,'Место для машины','Место для водителей, на своих машинах. 
-- Купив данное место вы увеличиваете колличество мест, которые могут купить не привязанные водители.'),
-- ('SRVPS3',1800,1500,TRUE,'CT1',0,'Продление договора','Для продления договора с FullDriver, требуется
-- продлить договор тех.обслуживания...');


-- --Таблица transaction_ps содержит информа. о транзакциях, которые были
-- --произведены
-- --"id" - уникальный индетификатор. (Уникальное, внешний ключ)
-- --"date_tr" - время проведения транзакции
-- --"services_ps" - сервис, который оплачивается
-- --"cabinet" - кабинет, который производит оплату
-- --"summ" - стоимость
-- DROP TABLE IF EXISTS transaction_ps;
-- CREATE TABLE transaction_ps (
-- id VARCHAR(15) NOT NULL PRIMARY KEY,
-- date_tr BIGINT NOT NULL,
-- services_ps VARCHAR(15) REFERENCES services_ps (id) ON DELETE SET NULL,
-- cabinet VARCHAR(15) REFERENCES cabinet_ps (id) ON DELETE SET NULL,
-- summ NUMERIC NOT NULL,
-- time_out BIGINT NOT NULL
-- );
-- --Последовательность для таблицы: transaction_ps
-- CREATE SEQUENCE transaction_ps_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
-- --Добавление транзакции партнеров
-- DROP FUNCTION IF EXISTS f_new_transaction_ps(_date_tr BIGINT, _services_ps VARCHAR(15), _cabinet VARCHAR(15), _summ NUMERIC, _time_out BIGINT);
-- CREATE OR REPLACE FUNCTION f_new_transaction_ps(_date_tr BIGINT, _services_ps VARCHAR(15), _cabinet VARCHAR(15), _summ NUMERIC, _time_out BIGINT) RETURNS VARCHAR(15) AS $$
-- DECLARE
-- _id VARCHAR(15);
-- _num_nextval VARCHAR(252);
-- BEGIN
-- _id = 'TRPS';
-- SELECT nextval('mark_id_seq') INTO _num_nextval;
-- _id = CONCAT(_id,_num_nextval);
-- INSERT INTO transaction_ps(id, date_tr, services_ps, cabinet, summ, time_out) VALUES(_id, _date_tr, _services_ps, _cabinet, _summ, _time_out);
-- RETURN _id;
-- END;
-- $$ LANGUAGE plpgsql;
-- --Default:
-- INSERT INTO transaction_ps VALUES 
-- ('TRPS1',1572117569,'SRVPS1','CBPS1',1500,1572123069);


--Таблица sessions сдержит информацию о сессиях для водителей
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"cabinet" - кабинет, который является автором сессии
--"duration" - длительность сессии
--"price" - цена сессии
--"active" - доступна ли сессия водителям
--"owndriver" - колличество водителей, которые могут купить сессию, до истечению её длительности.
DROP TABLE IF EXISTS sessions_dr;
CREATE TABLE sessions_dr (
id VARCHAR(15) NOT NULL PRIMARY KEY,
cabinet VARCHAR(15) REFERENCES cabinet_ps (id) ON DELETE SET NULL,
duration BIGINT NOT NULL,
price NUMERIC NOT NULL,
active BOOLEAN DEFAULT FALSE
);
--Последовательность для таблицы: session_dr
DROP SEQUENCE IF EXISTS session_dr_id_seq;
CREATE SEQUENCE session_dr_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Добавление сессии для водителей
DROP FUNCTION IF EXISTS f_new_session_dr(_cabinet VARCHAR(15), _duration BIGINT, _price NUMERIC);
CREATE OR REPLACE FUNCTION f_new_session_dr(_cabinet VARCHAR(15), _duration BIGINT, _price NUMERIC) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'SEDR';
SELECT nextval('session_dr_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
UPDATE sessions_dr SET active = FALSE WHERE active = TRUE AND id = _cabinet; 
IF (SELECT CASE WHEN COUNT(id) != 0 THEN TRUE ELSE FALSE END AS exist FROM sessions_dr WHERE cabinet = _cabinet AND duration = _duration AND price = _price) THEN
UPDATE sessions_dr SET active = TRUE WHERE cabinet = _cabinet AND duration = _duration AND price = _price;
RETURN 'EXIST';
END IF;
INSERT INTO sessions_dr(id, cabinet, duration, price, active) VALUES(_id, _cabinet, _duration, _price, TRUE);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Активация сессии f_enable_session
DROP FUNCTION IF EXISTS f_enable_session(_id VARCHAR(15));
CREATE OR REPLACE FUNCTION f_enable_session(_id VARCHAR(15)) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE sessions_dr SET active = FALSE WHERE active = TRUE; 
UPDATE sessions_dr SET active = TRUE WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--
DROP FUNCTION IF EXISTS f_disable_session(_id VARCHAR(15));
CREATE OR REPLACE FUNCTION f_enable_session(_id VARCHAR(15)) RETURNS BOOLEAN AS $$
DECLARE
BEGIN
UPDATE sessions_dr SET active = FALSE WHERE id = _id;
RETURN TRUE;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_session_dr('CBPS1',600,1);
SELECT f_new_session_dr('CBPS1',10800,170);
SELECT f_new_session_dr('CBPS1',18000,500);
SELECT * FROM sessions_dr;

--Таблица transaction_dr содержит информацию о транзакциях, которые были
--произведены водителями
--"id" - уникальный индетификатор. (Уникальное, внешний ключ)
--"date_tr" - время проведения транзакции
--"session_dr" - сессия, которую покупают
--"driver" - какой пользователь её покупает
--"summ" - сумма с учетом вычита суммы промокода (если он есть), но не меньше 0
--"promo_cod" - промокод, действует только на сесси от FullDriver
DROP TABLE IF EXISTS transaction_dr;
CREATE TABLE transaction_dr (
id VARCHAR(15) NOT NULL PRIMARY KEY,
date_tr BIGINT NOT NULL,
session_dr VARCHAR(15) REFERENCES sessions_dr (id) ON DELETE SET NULL,
driver VARCHAR(15) REFERENCES users (id) ON DELETE SET NULL,
summ DECIMAL(6,2) NOT NULL,
promocod VARCHAR(15),
status_pay VARCHAR(40) NOT NULL DEFAULT 'ANNOUNCED',
date_st BIGINT,
yandex_id VARCHAR(60)
);
--Последовательность для таблицы: transaction_dr
DROP SEQUENCE IF EXISTS transaction_dr_id_seq;
CREATE SEQUENCE transaction_dr_id_seq INCREMENT BY 1 NO MAXVALUE NO MINVALUE CACHE 1;
--Функция добовления транзакции
DROP FUNCTION IF EXISTS f_new_transaction_dr(_date_tr BIGINT, _session_dr VARCHAR(15), _driver VARCHAR(15), _summ INTEGER, _promocod VARCHAR(15), _status_pay VARCHAR(40));
CREATE OR REPLACE FUNCTION f_new_transaction_dr(_date_tr BIGINT, _session_dr VARCHAR(15), _driver VARCHAR(15), _summ INTEGER, _promocod VARCHAR(15), _status_pay VARCHAR(40)) RETURNS VARCHAR(15) AS $$
DECLARE
_id VARCHAR(15);
_num_nextval VARCHAR(252);
BEGIN
_id = 'TRDR';
SELECT nextval('mark_id_seq') INTO _num_nextval;
_id = CONCAT(_id,_num_nextval);
INSERT INTO transaction_dr(id,  date_tr, session_dr, driver, summ, promocod, status_pay) VALUES (_id, _date_tr, _session_dr, _driver, _summ, _promocod, _status_pay);
RETURN _id;
END;
$$ LANGUAGE plpgsql;
--Default:
SELECT f_new_transaction_dr(1572118402,'SEDR1','USR1',128.00,'745NV345A001');
SELECT f_new_transaction_dr(1584818297,'SEDR1','USR2',170.00,'no');
SELECT * FROM transaction_dr;