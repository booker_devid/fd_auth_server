import threading
import websocket
import ssl
import json
try:
    import thread
except ImportError:
    import _thread as thread
import time

def on_message(ws, message):
    print("Read - ", message)

def on_pong(ws, message):
    print("Read - ", message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    print("Up connect")
    def run(*args):
        time.sleep(15)
        print('Data driver_ready order send -> WS')
        data = {
        "type": "driver_ready",
        "data": ""
        }
        ata = json.dumps(data)
        ws.send(ata)
        time.sleep(5)
        print('Data recovery order send -> WS')
        data = {
        "type": "recovery",
        "data": {
            "price": 0,
            "comment": "client_true"
        }
        }
        ata = json.dumps(data)
        ws.send(ata)
        time.sleep(5)
        print('Data driver_ready order send -> WS')
        data = {
        "type": "driver_ready",
        "data": ""
        }
        ata = json.dumps(data)
        ws.send(ata)
        # data = {
        # "type": "end",
        # "data": ""
        # }
        # ata = json.dumps(data)
        # ws.send(ata)
        # print('Data end order send -> WS')
        time.sleep(60)
        print('Time out')
        ws.close()
        #print('Iteration - ' + str(i) + ', I sleep on 5....')
        while(True):
            com = input("Enter 'q' for exit >> ")
            if com == 'q':
                ws.close()
                break
            print("thread terminating...")
    thread.start_new_thread(run, ())

def new_con():
    #print(str(i) + ' - start!')
    websocket.enableTrace(True)
    ws = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
    ws = websocket.WebSocketApp("wss://scar.metro86.ru:4740/fd/order?token=a681e8843296199c883100327ce1c356922241a5409a732436dec1569e164bb7_1c43b846c8f32fc75bdfb49b1cafd35727f9877c9c241fa3d33e65fd96adb408_e81388cfea3cbab9c75e5905161c3b8c&driver=true",
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)

    try:
        ws.on_open = on_open
    except:
        print("error UpSocket")
    ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
    try:
        ws.on_open(ws)
    except:
        print("error UpSocket")

for i in range(1):
    time.sleep(0.1)
    print("start "+str(i))
    eth1 = threading.Thread(target=new_con, args=())
    eth1.start()