#!/bin/bash
#Default settings
PgNameHost="192.168.0.111"
PgNameUserDB="master"
PgPassUserDB="admin"
PgNameDB="fddb"
ServerListAddr="192.168.0.111"
ServerListPort="8080"
AsNameHost="127.0.0.1"
#--------------KEYS-------------
PgKeyRootCA="./key/postgres/fd_key/RootCA.crt"
PgKeyPub="./key/postgres/fd_key/Client.key"
PgSertPub="./key/postgres/fd_key/Client.crt"
ServerKey="./.key/server/Server.key"
ServerCert="./.key/server/Server.crt"
#Data of keys - /var/.sysk/*
ressl=true
ssl=X
#End settings
sys=true
while [ $sys == true ] #Server module
do
echo "|*****************************|"
echo "|********START*FD*************|"
echo "|*****************************|"
echo "|(Y) - RE/INSTALL             |"
echo "|(U) - UPDATE                 |"
echo "|(R) - CONFIGURATION          |"
echo "|(G) - GEN. SSL KEY           |"
echo "|(C) - CANCEL                 |"
echo "|*****************************|"
printf '\tCommand: '
read dokey
case $dokey in
	"Y"|"y")
		if [ ! -d ./fd_d ]
		then
			echo "Install fd_d"
		else
			echo "Reinstall fd_d"
			rm -rf ./fd_d
		fi
		mkdir ./tmp
		chmod -R 400 ./tmp
		cd ./tmp
		printf 'Master(default) if or name branch the other enter name git: '
		read dokey
		if [ ! -d ../fd_d/ ] #Checking directories on exist
		then
			mkdir ../fd_d
			chown fd:fd ../fd_d
			chmod -R 700 ../fd_d
		fi
		if [ -z $dokey ] #Master git
		then
			echo "DOWNLOAD master"
			git clone https://gitlab.com/three-developers/fd.git
		else
			echo "DOWNLOAD ${dokey}"
			git clone https://gitlab.com/three-developers/fd.git -b $dokey
		fi
		echo "build data"
		if [ `ls ./ | wc -l` -eq 0 ] #Empty dir
		then
			echo "ERROR BUILD OR AUTHORIZATION!"
			exit 25
		else
			/usr/local/go/bin/go build ./fd/src/main.go
			rm -rf ../fd_d/fd_d
			mv ./main ../fd_d/fd_d
			mv ./fd/database/genssl.sh ../fd_d/.genssl.sh
			chmod -R 740 ../fd_d/fd_d
			chmod -R 740 ../fd_d/.genssl.sh
		fi
		cd ../
		rm -rf ./tmp
		if [ ! -f ./fd_d/fd_d ] #Exist file
		then
			echo "ERROR MOVING"
			exit 25
		fi
		echo "SUCCESS INSTALL!"
		if [ -f ./fd_d/config.json ]
		then
			printf 'ERROR FILE config.json EXISTING... rewrite file? Y/N - '
			read rmrf
				case $rmrf in #Check of file
					"Y"|"y")
						rm -rf ./fd_d/config.json
						if [-f ./fd_d/config.json ]
						then
							echo "FILED REMOVE config.json"
							exit 25
						fi
						touch ./fd_d/config.json
						if [ ! -f ./fd_d/config.json ]
						then
							echo "FILED CREATE config.json"
							exit 25
						fi
						;;
					*)
						echo "The server has been SUCCESSFULLY UPDATED, but the configuration file has not been created or updated."
						exit 25
						;;
				esac
		fi
		echo "CONFIGURATION FILE config.json"
		echo "config.json (in []  parameters of default):"
		printf '\tPOSTGRES ip or domain host database [192.168.0.111] - '
		read t_namehost
		printf '\tPOSTGRES name user database [master] - '
		read t_nameuserdb
		printf '\tPOSTGRES pass for user database [admin] - '
		read t_passuserdb
		printf '\tPOSTGRES name database [fddb] - '
		read t_namedb
		printf '\tPATH to public key for TLS fd server [%s] - ' $ServerKey
		read t_ServerKey
		printf '\tPATH tp public sertificat for TLS fd server [%s] - ' $ServerCert
		read t_ServerCert
		printf '\tAEROSPIKE ip or domain host database [%s] - ' $AsNameHost
		read t_ashostname
			while [ $ressl == true ] #SSL module
			do
			printf 'enable SSL for PostgreSQL? Y/N - '
			read ssl
			case $ssl in
				"Y"|"y")
					printf '\tPATH to RootCA key for POSTGRES [%s] - ' $PgKeyRootCA
					read t_keyRootCA
					printf '\tPATH to public key for POSTGRES [%s] - ' $PgKeyPub
					read t_keyPub
					printf '\tPATH tp public sertificat for POSTGRES [%s] - ' $PgSertPub
					read t_sertPub
					#add checking of key and sertification
					ressl=false
					;;
				"N"|"n")
					ressl=false
					;;
			esac
			done
		printf '\tfd server listing address [192.168.0.111] - '
		read t_listaddr
		printf '\tfd server listing port [8080] - '
		read t_listport
		if [ ! -z $t_namehost ]
		then
			PgNameHost=$t_namehost
		fi
		if [ ! -z $t_nameuserdb ]
		then
			PgNameUserDB=$t_nameuserdb
		fi
		if [ ! -z $t_passuserdb ]
		then
			PgPassUserDB=$t_passuserdb
		fi
		if [ ! -z $t_namedb ]
		then
			PgNameDB=$t_namedb
		fi
		if [ ! -z $t_keyRootCA ]
		then
			PgKeyRootCA=$t_keyRootCA
		fi
		if [ ! -z $t_keyPub ]
		then
			PgKeyPub=$t_keyPub
		fi
		if [ ! -z $t_sertPub ]
		then
			PgSertPub=$t_sertPub
		fi
		if [ ! -z $t_listaddr ]
		then
			ServerListAddr=$t_listaddr
		fi
		if [ ! -z $t_listport ]
		then
			ServerListPort=$t_listport
		fi
		if [ ! -z $t_ServerKey ]
		then
			ServerKey=$t_listport
		fi
		if [ ! -z $t_ServerKey ]
		then
			ServerCert=$t_listport
		fi
		if [ ! -z $t_ashostname ]
		then
			AsNameHost=$t_ashostname
		fi
		#Configure config.json
		if [ $ssl == "N" ] || [ $ssl == "n" ]
		then
			echo "{" >> ./fd_d/config.json
			echo "\"conne_db_pg\":\"postgres://${PgNameUserDB}:${PgPassUserDB}@${PgNameHost}/${PgNameDB}?sslmode=disable\"," >> ./fd_d/config.json
			echo "\"conne_db_as\":\"${AsNameHost}\"," >> ./fd_d/config.json
			echo "\"address_server\":\"${ServerListAddr}:${ServerListPort}\"," >> ./fd_d/config.json
			echo "\"certFile\":\"${ServerCert}\"," >> ./fd_d/config.json
			echo "\"keyFile\":\"${ServerKey}\"" >> ./fd_d/config.json
			echo "}" >> ./fd_d/config.json
		else
			echo "{" >> ./fd_d/config.json
			echo "\"conne_db_pg\":\"postgres://${PgNameUserDB}:${PgPassUserDB}@${PgNameHost}/${PgNameDB}?sslmode=verify-full?sslcert=${PgSertPub}?sslkey=${PgKeyPub}?sslrootcert=${PgKeyRootCA}\"," >> ./fd_d/config.json
			echo "\"conne_db_as\":\"${AsNameHost}\"," >> ./fd_d/config.json
			echo "\"address_server\":\"${ServerListAddr}:${ServerListPort}\"," >> ./fd_d/config.json
			echo "\"certFile\":\"${ServerCert}\"," >> ./fd_d/config.json
			echo "\"keyFile\":\"${ServerKey}\"" >> ./fd_d/config.json
			echo "}" >> ./fd_d/config.json
		fi
		chown fd:fd ./fd_d/config.json
		chmod -R 400 ./fd_d/config.json
		echo "SUCCESS MAKE SETTINGS OF config.json"
		sys=flase
		;;
	"U"|"u")
		echo "Update fd_d..."
		if [ ! -d ./fd_d/ ] #Checking directories on exist
		then
			echo "fd_d not found, please restart script RE/INSTALL (Y)"
			exit 25
		fi
		mkdir ./tmp
		chmod -R 400 ./tmp
		cd ./tmp
		printf 'Master(default) if or name branch the other enter name git: '
		read dokey
		if [ -z $dokey ] #Master git
		then
			echo "DOWNLOAD master"
			git clone https://gitlab.com/three-developers/fd.git
		else
			echo "DOWNLOAD ${dokey}"
			git clone https://gitlab.com/three-developers/fd.git -b $dokey
		fi
		echo "build data"
		if [ `ls ./ | wc -l` -eq 0 ] #Empty dir
		then
			echo "ERROR BUILD OR AUTHORIZATION!"
			exit 25
		else
			/usr/local/go/bin/go build ./fd/src/main.go
			rm -rf ../fd_d/fd_d
			mv ./main ../fd_d/fd_d
			mv ./fd/database/genssl.sh ../fd_d/.genssl.sh
			chmod -R 740 ../fd_d/fd_d
			chmod -R 740 ../fd_d/.genssl.sh
		fi
		cd ../
		rm -rf ./tmp
		if [ ! -f ./fd_d/fd_d ] #Exist file
		then
			echo "ERROR MOVING"
			exit 25
		fi
		echo "SUCCESS UPDATE!"
		sys=false
		;;
	"R"|"r")
		if [ ! -d ../fd_d/ ] #Checking directories on exist
		then
			echo "fd_d not found, please restart script RE/INSTALL (Y)"
			exit 25
		fi
		echo "CONFIGURATION FILE config.json"
		rm -rf ./fd_d/config.json
		if [ -f ./fd_d/config.json ]
		then
			echo "FILED REMOVE config.json"
			exit 25
		fi
		touch ./fd_d/config.json
		if [ ! -f ./fd_d/config.json ]
		then
			echo "FILED CREATE config.json"
			exit 25
		fi
		echo "CONFIGURATION FILE config.json"
		echo "config.json (in []  parameters of default):"
		printf '\tPOSTGRES ip or domain host database [192.168.0.111] - '
		read t_namehost
		printf '\tPOSTGRES name user database [master] - '
		read t_nameuserdb
		printf '\tPOSTGRES pass for user database [admin] - '
		read t_passuserdb
		printf '\tPOSTGRES name database [fddb] - '
		read t_namedb
		printf '\tPATH to public key for TLS fd server [%s] - ' $ServerKey
		read t_ServerKey
		printf '\tPATH tp public sertificat for TLS fd server [%s] - ' $ServerCert
		read t_ServerCert
		printf '\tAEROSPIKE ip or domain host database [%s] - ' $AsNameHost
		read t_ashostname
			while [ $ressl == true ] #SSL module
			do
			printf 'enable SSL for PostgreSQL? Y/N - '
			read ssl
			case $ssl in
				"Y"|"y")
					echo "*it is RECOMMEDNDED to leave the DEFAULT data*"
					printf '\tPATH to RootCA key for POSTGRES [%s] - ' $PgKeyRootCA
					read t_keyRootCA
					printf '\tPATH to public key for POSTGRES [%s] - ' $PgKeyPub
					read t_keyPub
					printf '\tPATH tp public sertificat for POSTGRES [%s] - ' $PgSertPub
					read t_sertPub
					ressl=false
					;;
				"N"|"n")
					ressl=false
					;;
			esac
			done
		printf '\tfd server listing address [192.168.0.111] - '
		read t_listaddr
		printf '\tfd server listing port [8080] - '
		read t_listport
		if [ ! -z $t_namehost ]
		then
			PgNameHost=$t_namehost
		fi
		if [ ! -z $t_nameuserdb ]
		then
			PgNameUserDB=$t_nameuserdb
		fi
		if [ ! -z $t_passuserdb ]
		then
			PgPassUserDB=$t_passuserdb
		fi
		if [ ! -z $t_namedb ]
		then
			PgNameDB=$t_namedb
		fi
		if [ ! -z $t_keyRootCA ]
		then
			PgKeyRootCA=$t_keyRootCA
		fi
		if [ ! -z $t_keyPub ]
		then
			PgKeyPub=$t_keyPub
		fi
		if [ ! -z $t_sertPub ]
		then
			PgSertPub=$t_sertPub
		fi
		if [ ! -z $t_listaddr ]
		then
			ServerListAddr=$t_listaddr
		fi
		if [ ! -z $t_listport ]
		then
			ServerListPort=$t_listport
		fi
		if [ ! -z $t_ServerKey ]
		then
			ServerKey=$t_listport
		fi
		if [ ! -z $t_ServerKey ]
		then
			ServerCert=$t_listport
		fi
		if [ ! -z $t_ashostname ]
		then
			AsNameHost=$t_ashostname
		fi
		#Configure config.json
		if [ $ssl == "N" ] || [ $ssl == "n" ]
		then
			echo "{" >> ./fd_d/config.json
			echo "\"conne_db_pg\":\"postgres://${PgNameUserDB}:${PgPassUserDB}@${PgNameHost}/${PgNameDB}?sslmode=disable\"," >> ./fd_d/config.json
			echo "\"conne_db_as\":\"${AsNameHost}\"," >> ./fd_d/config.json
			echo "\"address_server\":\"${ServerListAddr}:${ServerListPort}\"," >> ./fd_d/config.json
			echo "\"certFile\":\"${ServerCert}\"," >> ./fd_d/config.json
			echo "\"keyFile\":\"${ServerKey}\"" >> ./fd_d/config.json
			echo "}" >> ./fd_d/config.json
		else
			echo "{" >> ./fd_d/config.json
			echo "\"conne_db_pg\":\"postgres://${PgNameUserDB}:${PgPassUserDB}@${PgNameHost}/${PgNameDB}?sslmode=verify-full?sslcert=${PgSertPub}?sslkey=${PgKeyPub}?sslrootcert=${PgKeyRootCA}\"," >> ./fd_d/config.json
			echo "\"conne_db_as\":\"${AsNameHost}\"," >> ./fd_d/config.json
			echo "\"address_server\":\"${ServerListAddr}:${ServerListPort}\"," >> ./fd_d/config.json
			echo "\"certFile\":\"${ServerCert}\"," >> ./fd_d/config.json
			echo "\"keyFile\":\"${ServerKey}\"" >> ./fd_d/config.json
			echo "}" >> ./fd_d/config.json
		fi
		chown fd:fd ./fd_d/config.json
		chmod -R 400 ./fd_d/config.json
		sys=false
		;;
	"G"|"g")
		echo "Generated keys..."
		if [ ! -d ./fd_d]
		then
			echo "fd_d not found, please restart script RE/INSTALL (Y)"
			exit 25
		fi
		cd ./fd_d
		if [ -d ./.key ]
		then
			echo "Key rewrite!"
			rm -rf ./.key
		fi
		mkdir ./.key
		cd ./.key
		mkdir {postgres,server}
		if [ ! -d /var/.sysk ]
		then
			echo "NOT FOUND DIRECTORY .SYSK, CREATE DIRECTORY!"
			echo "GEN KEY FOR DATABASE - PostgreSQL"
			mkdir -p /var/.sysk/postgres
			cp ../.genssl.sh /var/.sysk/postgres/genssl.sh
			sh /var/.sysk/postgres/genssl.sh
		fi
		cp  /var/.sysk/postgres/RootCA.crt ./postgres/RootCA.crt
		if [ -f ./postgres/RootCA.crt ]
		then
			echo "SECCESS - /postgres/RootCA.crt"
		else
			echo "ERROR - /postgres/RootCA.crt"
		fi
		cp  /var/.sysk/postgres/Client.crt ./postgres/Client.crt
		if [ -f ./postgres/Client.crt ]
		then
			echo "SECCESS - /postgres/Client.crt"
		else
			echo "ERROR - /postgres/Client.crt"
		fi
		cp  /var/.sysk/postgres/Client.key ./postgres/Client.key
		if [ -f ./postgres/Client.key ]
		then
			echo "SECCESS - /postgres/Client.key"
		else
			echo "ERROR - /postgres/Client.key"
		fi
		echo "*******************************************************************"
		echo "*----* *----* *----* *----*    SERVER    *----* *----* *----* *---*"
		echo "*******************************************************************"
		cd server
		openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out Server.crt -keyout Server.key
		cd ../
		if [ -f ./server/Server.crt ]
		then
			echo "SECCESS - /server/Server.crt"
		else
			echo "ERROR - /server/Server.crt"
		fi
		if [ -f ./server/Server.key ]
		then
			echo "SECCESS - /server/Server.key"
		else
			echo "ERROR - /server/Server.key"
		fi
		chown -R fd:fd ../.key
		chmod -R 600 ../.key
		sys=false
		;;
	"C"|"c")
		echo "Cancel script"
		sys=false
		;;
esac
done
echo "END fd SCRIPT!"