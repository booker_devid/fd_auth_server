# osm2pgsql -d map1 -c -U root --slim -C 10000 --number-processes 5 ural-fed-district-latest.osm.pbf
import os
import sys
import git
import yaml
import json
import time
import gitlab
import shutil
from threading import Thread
import codecs
tx_res = "\u001b[0m"
tx_def = "\u001b[39m"
tx_red = "\u001b[31m"
tx_green = "\u001b[32m"
tx_yellow = "\u001b[33m"
tx_blue = "\u001b[34m"
te_up = "\033[2A"
te_ca = "\033c"
#Location settings
directory = "/opt/fulldriver"
dir_servers = directory+"/fdd"
dir_auth = directory+"/fdd_auth"
dir_source = directory+"/tmp"
dir_sfdd = directory+"/tmp/fdd"
dir_sfdd_auth = directory+"/tmp/fdd_auth"
dir_sk = directory+"/fd_kfsc"
dir_s = directory+"/fd_server"
dir_sa = directory+"/fd_auth"
directname = [dir_sa,dir_s,dir_sk]
conf_name = "/config.yml"
id_project_ffd = "17912715"
id_project_ffd_auth = "17912679"
##**SERVER**CONIGURATION**##
#Postgres settings
postgres_host = "192.168.0.111:5432"
postgres_login = "master"
postgres_pwd = "admin"
postgres_sslmode = "?sslmode=disable"
postgres_db = "test_fddb"
#postgres_cert = ""
postgres_max_connect = "&pool_max_conns=10"
#Server settings
host_addr = "192.168.0.111"
host_port_start = 4741
host_cert = "../../fd_kfsc/certificate.crt"
host_key = "../../fd_kfsc/private.pem"
#SCAR server
scar_addr = "192.168.0.111:8080"
scar_cert = "../../fd_kfsc/Server.crt"
scar_key = "389abf937a80d6ef3c6df67df2f265a5fc682d536bb23535a3d9decc9ab34baa"
scar_id = "SFD"
############################
list_host_for_proxy = []
class GitSystem():
    UID = str
    secret = str
    login = str
    pwd = str
    repo = str
    token = str
    fdd = str
    fdd_auth = str
    git = gitlab.Gitlab
    def CheckUID(self):
        print("Check Git UID - " + self.UID)

GS = GitSystem()

def CheckLocation():
    print("Check directory")
    check_file = False
    if os.path.exists(directory) != True:
        try:
            os.mkdir(directory)
        except OSError:
            OutWarning("Directory %s by is't create" % directory)
            return "Err Check Location..."
        else:
            OutGood("Directory %s by is success create" % directory)
            return ""
    folders = os.listdir(directory)
    print("Folders:")
    for check in range(3):
        for i in folders:
            sys.stdout.write(". " + directname[check])
            if i.title == directname[check]:
                sys.stdout.write("\r")
                OutGoodProcess(directname[check])
                check_file = True
                continue
        if check_file != True:
            sys.stdout.write("\r")
            OutBadProcess(directname[check]) 
    return ""

def ReadConfiguration():
    print("Read configuration...")
    path = os.getcwd()
    path_config = path+conf_name
    print("Config path = ", path_config)
    if os.path.exists(path_config) != True:
        return "Err read config file ..."
    with open(path_config) as f:
        templates = yaml.safe_load(f)
        if templates['git_id'] == '':
            OutBadProcess("git_id")
        else:
            GS.UID = templates['git_id']
            OutGoodProcess("git_id")
        if templates['git_secret'] == '':
            OutBadProcess("git_secret")
        else:
            GS.secret = templates['git_secret']
            OutGoodProcess("git_secret")
        if templates['git_repo'] == '':
            OutBadProcess("git_repo")
        else:
            GS.repo = templates['git_repo']
            OutGoodProcess("git_repo")
        if templates['git_fdd'] == '':
            OutBadProcess("git_fdd")
        else:
            GS.fdd = templates['git_fdd']
            OutGoodProcess("git_fdd")
        if templates['git_fdd_auth'] == '':
            OutBadProcess("git_fdd_auth")
        else:
            GS.fdd_auth = templates['git_fdd_auth']
            OutGoodProcess("git_fdd_auth")
        if templates['git_token'] == '':
            OutBadProcess("git_token")
        else:
            GS.token = templates['git_token']
            OutGoodProcess("git_token")
    return ""

def AuthGit():
    print("Connection GitLab...")
    GS.git = gitlab.Gitlab("https://gitlab.com/",oauth_token=GS.token)
    GS.git.auth()
    return ""

def OutUser(str):
    print("~ "+str)

def OutError(str):
    print(tx_red+"! "+str+tx_def)

def OutWarning(str):
    print(tx_yellow+"! "+str+tx_def)

def OutGood(str):
    print(tx_green+" "+str+tx_def)    

def OutBadProcess(str):
    print(tx_red+"\u274C "+str+tx_def)

def OutGoodProcess(str):
    print(tx_green+"\u2713 "+str+tx_def)

def InfoServers():
    check_fdd_auth = False
    check_fdd_servers = False
    if os.path.exists(dir_auth) != True:
        OutWarning(tx_red+"fdd_auth"+tx_def+" server not found %s path... " % dir_auth)
    if os.path.exists(dir_auth) != True:
        OutWarning(tx_red+"fdd"+tx_def+" servers not found %s path... " % dir_servers)
    if check_fdd_auth == False and check_fdd_servers == False:
        OutUser("Not fount installed servers.")
        OutUser("Start script on using host...")
    
    input("~ Enter...")

def TempProcess(str):
    while(True):
        sys.stdout.write("\r")
        sys.stdout.write("-")
        time.sleep(0.3)
        sys.stdout.write("\r")
        sys.stdout.write("\\")
        time.sleep(0.3)
        sys.stdout.write("\r")
        sys.stdout.write("|")
        time.sleep(0.3)
        sys.stdout.write("\r")
        sys.stdout.write("/")
        time.sleep(0.3)

def LoadClaster():
    OutUser("Start load claster...")
    try:
        if os.path.exists(dir_source) != True:
            os.mkdir(dir_source)
        if os.path.exists(dir_sfdd) != True:
            os.mkdir(dir_sfdd)
            if os.path.exists(dir_sfdd) != True:
                OutGoodProcess("Успешно создана директория: "+dir_sfdd)
    except OSError:
        OutError("Directory by is't create: " +dir_sfdd)
        return "Err Check Location..."
    OutUser("Repo: ")
    p = GS.git.projects.get(id_project_ffd)
    branches = p.branches.list()
    for idx,br in enumerate(branches):
        print(str(idx+1) + ".- " + br.name)
    num_brench = int(input("Number brench >> "))
    if num_brench > 0:
        num_brench =-1
    else:
        OutError("Unknow branch")
    OutGood("Starting ... git clone https://"+GS.UID+":"+GS.secret+"@gitlab.com/three-developers/"+GS.fdd+" -b "+branches[num_brench].name+" "+dir_sfdd)
    os.system("git clone https://"+GS.UID+":"+GS.secret+"@gitlab.com/three-developers/"+GS.fdd+" -b "+branches[num_brench].name+" "+dir_sfdd)
    os.system("go build -o "+directory+"/main "+dir_sfdd+"/src/main.go")
    OutGood("go build -o "+directory+"/main"+dir_sfdd+"/src/main.go")
    if os.path.exists(directory+"/main") != True:
        return "Error test build project..."
    else:
        os.mkdir(directory+"/fd_server")
    os.system("git clone https://"+GS.UID+":"+GS.secret+"@gitlab.com/three-developers/kfds_secure.git "+dir_sk)
    if os.path.exists(dir_sk) != True:
        OutError("Error create dir "+dir_sk)
    num_servers = int(input("Num servers FDD: "))
    for idx in range(num_servers):
        tmp_dir = directory+"/fd_server/fdd"+str(idx+1)
        if os.path.exists(tmp_dir):
            OutWarning("Server"+str(idx+1)+" exists...")
            continue
        os.mkdir(tmp_dir)
        if os.path.exists(directory+"/main"):
            OutWarning("copy "+directory+"/main "+tmp_dir)
            shutil.copyfile(directory+"/main",tmp_dir+"/fdd"+str(idx+1))
            os.system("chmod +x "+tmp_dir+"/fdd"+str(idx+1))
        if os.path.exists(tmp_dir+"/fdd"+str(idx+1)) != True:
            sys.stdout.write("\r")
            OutError("Error copy data server"+str(idx+1)+" in dir "+tmp_dir)
            continue
        else:
            OutGoodProcess("Success copy data server"+str(idx+1))
        OutWarning("config for server"+str(idx+1))
        config = {
            "conne_db_pg":"postgres://"+postgres_login+":"+postgres_pwd+"@"+postgres_host+"/"+postgres_db+postgres_sslmode+postgres_max_connect,
            "address_server":host_addr+":"+str(host_port_start+idx+1),
            "certFile":host_cert,
            "keyFile":host_key,
            "scarAddr":scar_addr,
            "scarCrt":scar_cert,
            "scarKey":scar_key,
            "scarId":scar_id+str(idx+1)
        }
        list_host_for_proxy.append(host_addr+":"+str(host_port_start+idx))
        with open(tmp_dir+'/config.json', 'w', encoding='utf-8') as f:
            json.dump(config, f, ensure_ascii=False)
        if os.path.exists(tmp_dir+"/config.json"):
            OutGoodProcess("Success configuration server"+str(idx+1))
    OutWarning("info for proxy ...")
    proxy = {
        "hosts":list_host_for_proxy
    }
    with open(directory+'/proxy.json', 'w', encoding='utf-8') as f:
            json.dump(proxy, f, ensure_ascii=False)
    if os.path.exists(directory+'/proxy.json'):
            OutGoodProcess("Success configuration proxy"+str(idx+1))
    os.remove(directory+"/main")
    return ""

def LoadAuth():
    OutUser("Start load claster...")
    try:
        if os.path.exists(dir_source) != True:
            os.mkdir(dir_source)
        if os.path.exists(dir_sfdd_auth) != True:
            os.mkdir(dir_sfdd_auth)
            if os.path.exists(dir_sfdd_auth) != True:
                OutGoodProcess("Success create: "+dir_sfdd_auth)
    except OSError:
        OutError("Can't create directory: " +dir_sfdd_auth)
        return "Err Check Location..."
    OutUser("Repo...: ")
    p = GS.git.projects.get(id_project_ffd_auth)
    branches = p.branches.list()
    for idx,br in enumerate(branches):
        print(str(idx+1) + ".- " + br.name)
    num_brench = int(input("Number brench >> "))
    if num_brench > 0:
        num_brench =-1
    else:
        OutError("Unknow branch")
    OutGood("Starting git clone ...."+GS.fdd_auth+" -b "+branches[num_brench].name+" "+dir_sfdd_auth)
    os.system("git clone https://"+GS.UID+":"+GS.secret+"@gitlab.com/three-developers/"+GS.fdd_auth+" -b "+branches[num_brench].name+" "+dir_sfdd_auth)
    os.system("go build -o "+directory+"/main "+dir_sfdd_auth+"/src/main.go")
    OutGood("go build -o "+directory+"/main "+dir_sfdd_auth+"/src/main.go")
    if os.path.exists(directory+"/main") != True:
        return "Error test build project..."
    else:
        if os.path.exists(directory+"/fd_auth") != True:
            os.mkdir(directory+"/fd_auth")
    if os.path.exists(dir_sk) != True:
        os.mkdir(dir_sk)
        os.system("git clone https://"+GS.UID+":"+GS.secret+"@gitlab.com/three-developers/kfds_secure.git "+dir_sk)
    tmp_dir = directory+"/fd_auth/fdd_auth"
    if os.path.exists(tmp_dir):
        return "Err server 1 exists..."
    os.mkdir(tmp_dir)
    if os.path.exists(directory+"/main"):
        OutWarning("copy "+directory+"/main "+tmp_dir)
        shutil.copyfile(directory+"/main",tmp_dir+"/fdd_auth")
        os.system("chmod +x "+tmp_dir+"/fdd_auth")
    if os.path.exists(tmp_dir+"/fdd_auth") != True:
        sys.stdout.write("\r")
        return "Error copy data server in dir "+tmp_dir
    else:
        OutGoodProcess("Success copy data server")
    OutWarning("config for server")
    config = {
        "conne_db_pg":"postgres://"+postgres_login+":"+postgres_pwd+"@"+postgres_host+"/"+postgres_db+postgres_sslmode+postgres_max_connect,
        "address_server":host_addr+":"+str(host_port_start),
        "certFile":host_cert,
        "keyFile":host_key,
        "scarAddr":scar_addr,
        "scarCrt":scar_cert,
        "scarKey":scar_key,
        "scarId":scar_id+'0'
    }
    list_host_for_proxy.append(host_addr+":"+str(host_port_start))
    with open(tmp_dir+'/config.json', 'w', encoding='utf-8') as f:
        json.dump(config, f, ensure_ascii=False)
    if os.path.exists(tmp_dir+"/config.json"):
        OutGoodProcess("Success configuration server"+str(idx+1))
    OutWarning("info for proxy ...")
    proxy = {
        "hosts":list_host_for_proxy
    }
    with open(directory+'/proxy_auth.json', 'w', encoding='utf-8') as f:
            json.dump(proxy, f, ensure_ascii=False)
    if os.path.exists(directory+'/proxy_auth.json'):
            OutGoodProcess("Success configuration proxy"+str(idx+1))
    os.remove(directory+"/main")
    return ""

#Starting
print(tx_blue+"Full Driver"+tx_def+" script...")
#Check Location
err = CheckLocation()
if len(err) != 0:
    OutError(err)
    os.abort()
#Read configuration
err = ReadConfiguration()
if len(err) != 0:
    OutError(err)
    os.abort()
#Connection Gitlab
err = AuthGit()
if len(err) != 0:
    OutError(err)
    os.abort()
while(True):
    print("\n\n_________________________\n")
    com = input(tx_yellow+"Menu:"+tx_def+"\nl.- Load claster\nu.- Update claster\na.- Authorization server\n"+tx_green+"i.- Inforvation servers\n"+tx_red+"q.- Exit"+tx_def+"\n_________________________\nEnter section menu >> ")
    if com == 'q':
        OutUser("Exit!")
        os.abort()
    
    if com == 'l':
        err = LoadClaster()
        if len(err) != 0:
            OutError(err)

    if com == 'a':
        err = LoadAuth()
        if len(err) != 0:
            OutError(err)

    if com == 'i':
        InfoServers()
